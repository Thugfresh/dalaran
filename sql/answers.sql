-- question #8
-- ANSWER: 2021-01-01
select day_dt
from test.time 
where not time_crnt_f = 'N'
and time_lvl = 'Day'
order by time_key desc
limit 1


-- question #9
-- ANSWER: 8 (assuming this means inclusive of the current date, otherwise the answer is 7)
select COUNT (*) as assets_since_current_date
from (select * from test.asset) a
inner join (
	select time_key as tk
	from test.time
	where not time_crnt_f = 'N'
	and time_lvl = 'Day'
	order by time_key desc
	limit 1
) t
on (a.time_key <= t.tk)

-- question #10
-- ANSWER: (Pole: 2, Transformer: 1, Switch: 1, Cable: 4) if inclusive, if not inclusive then one less pole
select 
	sum(a.poles) as poles,
	sum(a.transformers) as transformers,
	sum(a.switches) as switches,
	sum(a.cables) as cables
from (
	select time_key,
		sum(case when asset_class = 'Pole' then 1 else 0 end) as poles,
		sum(case when asset_class = 'Transformer' then 1 else 0 end) as transformers,
		sum(case when asset_class = 'Switch' then 1 else 0 end) as switches,
		sum(case when asset_class = 'Cable' then 1 else 0 end) as cables
	from test.asset
	group by time_key
) a
inner join (
	select time_key as tk
	from test.time
	where not time_crnt_f = 'N'
	and time_lvl = 'Day'
	order by time_key desc
	limit 1
) t
on (a.time_key <= t.tk)

-- question #11
-- ANSWER: I found this question confusing, because the year for 2020 has a higher time_key
-- than jan 2021, making it difficult to determine if something was in 2021 or in 2020
-- when comparing the time_key


-- question #12
-- ANSWER: I find this question confusing. tehcnically all the assets exist in 2021. if my
--         understanding of the schema is correct. If the question presented instead means
--         how many assets were created in 2021 for each classification would be 0, because
--         no assets were created in 2021
select 
	sum(a.poles) as poles,
	sum(a.transformers) as transformers,
	sum(a.switches) as switches,
	sum(a.cables) as cables
from (
	select time_key,
		sum(case when asset_class = 'Pole' then 1 else 0 end) as poles,
		sum(case when asset_class = 'Transformer' then 1 else 0 end) as transformers,
		sum(case when asset_class = 'Switch' then 1 else 0 end) as switches,
		sum(case when asset_class = 'Cable' then 1 else 0 end) as cables
	from test.asset
	group by time_key
) a
inner join (
	select time_key as tk
	from test.time 
	where not time_crnt_f = 'N'
	and time_lvl = 'Year'
    -- something created in 2021 would have a time_key > the one in 2020?
	and anul_clndr_code = '2020'
	order by time_key desc
	limit 1
) t
on (a.time_key > t.tk)

-- question # 13
-- ANSWER: In the time table the following field was set to an incorrect siae
--         DAY_OF_WK_CODE    CHAR(1)
--
--         This should instead be
--         DAY_OF_WK_CODE    CHAR(10)
--
--         to account for the days of the week being longer than 1 character.
--
--         the correct schema is provided below:
CREATE SCHEMA test;
	
CREATE TABLE test.TIME			
(			
    TIME_KEY          INTEGER,			
    TIME_LVL          CHAR(15),			
    TIME_CRNT_F       CHAR(1),			
    ANUL_CLNDR_CODE   CHAR(4),			
    MO_CLNDR_CODE     CHAR(10),			
    DAY_OF_WK_CODE    CHAR(14),			
    DAY_DT            DATE,			
    TIME_DESC         VARCHAR(70),			
    YR_KEY	            INTEGER,
    MO_KEY            INTEGER,			
    DAY_KEY           INTEGER			
);		

CREATE TABLE test.ASSET	
(	
    TIME_KEY          INTEGER,	
    ASSET_ID          INTEGER,	
    ASSET_CLASS       VARCHAR(70),	
    ASSET_NAME        VARCHAR(70),	
    ASSET_CONDITION   INTEGER,	
    ASSET_AGE         INTEGER	
);