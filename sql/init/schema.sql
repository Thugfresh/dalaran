CREATE SCHEMA test;
	
CREATE TABLE test.TIME			
(			
    TIME_KEY          INTEGER,			
    TIME_LVL          CHAR(15),			
    TIME_CRNT_F       CHAR(1),			
    ANUL_CLNDR_CODE   CHAR(4),			
    MO_CLNDR_CODE     CHAR(10),			
    DAY_OF_WK_CODE    CHAR(14),			
    DAY_DT            DATE,			
    TIME_DESC         VARCHAR(70),			
    YR_KEY	            INTEGER,
    MO_KEY            INTEGER,			
    DAY_KEY           INTEGER			
);		

CREATE TABLE test.ASSET	
(	
    TIME_KEY          INTEGER,	
    ASSET_ID          INTEGER,	
    ASSET_CLASS       VARCHAR(70),	
    ASSET_NAME        VARCHAR(70),	
    ASSET_CONDITION   INTEGER,	
    ASSET_AGE         INTEGER	
);	

INSERT INTO test.asset(
	time_key, asset_id, asset_class, asset_name, asset_condition, asset_age)
	VALUES (15751,1,'Pole','Wood Pole',5,20);
	
INSERT INTO test.asset(
	time_key, asset_id, asset_class, asset_name, asset_condition, asset_age)
	VALUES (15752,1,'Pole','Wood Pole',5,21);
	
INSERT INTO test.asset(
	time_key, asset_id, asset_class, asset_name, asset_condition, asset_age)
	VALUES (15748,2,'Transformer','Overhead Transformer',10,10);	
		
INSERT INTO test.asset(
	time_key, asset_id, asset_class, asset_name, asset_condition, asset_age)
	VALUES (15749,3,'Switch','Network Switch',4,10);	
	
INSERT INTO test.asset(
	time_key, asset_id, asset_class, asset_name, asset_condition, asset_age)
	VALUES (15748,4,'Cable','Cable',4,20);	
	
	
INSERT INTO test.asset(
	time_key, asset_id, asset_class, asset_name, asset_condition, asset_age)
	VALUES (15748,5,'Cable','Cable',7,20);	
	
		
INSERT INTO test.asset(
	time_key, asset_id, asset_class, asset_name, asset_condition, asset_age)
	VALUES (15748,6,'Cable','Cable',4,10);	
	
	
INSERT INTO test.asset(
	time_key, asset_id, asset_class, asset_name, asset_condition, asset_age)
	VALUES (15748,7,'Cable','Cable',1,40);		

		
INSERT INTO test."time"(
	time_key, time_lvl, time_crnt_f, anul_clndr_code, mo_clndr_code, day_of_wk_code, day_dt, time_desc, yr_key, mo_key, day_key)
	VALUES (15748,'Day'            ,'Y',2020,'December'  ,'Monday'    ,'20201228','12/28/2020',15758,15756,15748);
	
INSERT INTO test."time"(
	time_key, time_lvl, time_crnt_f, anul_clndr_code, mo_clndr_code, day_of_wk_code, day_dt, time_desc, yr_key, mo_key, day_key)
	VALUES (15749,'Day'            ,'Y',2020,'December'  ,'Tuesday'   ,'20201229','12/29/2020',15758,15756,15749);
	
	INSERT INTO test."time"(
	time_key, time_lvl, time_crnt_f, anul_clndr_code, mo_clndr_code, day_of_wk_code, day_dt, time_desc, yr_key, mo_key, day_key)
	VALUES (15750,'Day'            ,'Y',2020,'December'  ,'Wednesday' ,'20201230','12/30/2020',15758,15756,15750);
	
	INSERT INTO test."time"(
	time_key, time_lvl, time_crnt_f, anul_clndr_code, mo_clndr_code, day_of_wk_code, day_dt, time_desc, yr_key, mo_key, day_key)
	VALUES (15751,'Day'            ,'Y',2020,'December'  ,'Thursday' ,'20201231','12/31/2020',15758,15756,15751);
	
		
	INSERT INTO test."time"(
	time_key, time_lvl, time_crnt_f, anul_clndr_code, mo_clndr_code, day_of_wk_code, day_dt, time_desc, yr_key, mo_key, day_key)
	VALUES (15752,'Day'            ,'Y',2021,'January','Friday','20210101','01/01/2021',16238,15862,15752);
	
	INSERT INTO test."time"(
	time_key, time_lvl, time_crnt_f, anul_clndr_code, mo_clndr_code, day_of_wk_code, day_dt, time_desc, yr_key, mo_key, day_key)
	VALUES (15753,'Day'            ,'N',2021,'January','Saturday','20210102','01/02/2021',16238,15862,15753);
	
	INSERT INTO test."time"(
	time_key, time_lvl, time_crnt_f, anul_clndr_code, mo_clndr_code, day_of_wk_code, day_dt, time_desc, yr_key, mo_key, day_key)
	VALUES (15676,'Month','Y',2020,'October','',null,'',15758,15676,null );
	
	INSERT INTO test."time"(
	time_key, time_lvl, time_crnt_f, anul_clndr_code, mo_clndr_code, day_of_wk_code, day_dt, time_desc, yr_key, mo_key, day_key)
	VALUES (15716,'Month'          ,'Y',2020,'November'  ,'',null,'',15758,15716,null);
	
	INSERT INTO test."time"(
	time_key, time_lvl, time_crnt_f, anul_clndr_code, mo_clndr_code, day_of_wk_code, day_dt, time_desc, yr_key, mo_key, day_key)
	VALUES (15756,'Month'          ,'Y',2020,'December'  ,'',null,'',15758,15756,null);
	
	INSERT INTO test."time"(
	time_key, time_lvl, time_crnt_f, anul_clndr_code, mo_clndr_code, day_of_wk_code, day_dt, time_desc, yr_key, mo_key, day_key)
	VALUES (15862,'Month'          ,'Y',2021,'January','',null,'',16238,15862,null);
	
	INSERT INTO test."time"(
	time_key, time_lvl, time_crnt_f, anul_clndr_code, mo_clndr_code, day_of_wk_code, day_dt, time_desc, yr_key, mo_key, day_key)
	VALUES (15973,'Month'          ,'N',2021,'February','',null,'',16238,15973,null);
	
	INSERT INTO test."time"(
	time_key, time_lvl, time_crnt_f, anul_clndr_code, mo_clndr_code, day_of_wk_code, day_dt, time_desc, yr_key, mo_key, day_key)
	VALUES (15278,'Year'           ,'Y',2019,'','',null,'',15278,null,null);
	
	INSERT INTO test."time"(
	time_key, time_lvl, time_crnt_f, anul_clndr_code, mo_clndr_code, day_of_wk_code, day_dt, time_desc, yr_key, mo_key, day_key)
	VALUES (15758,'Year'           ,'Y',2020,'','',null,'',15758,null,null);
	
	
	INSERT INTO test."time"(
	time_key, time_lvl, time_crnt_f, anul_clndr_code, mo_clndr_code, day_of_wk_code, day_dt, time_desc, yr_key, mo_key, day_key)
	VALUES (16238,'Year','Y',2021,'','',null,'',16238,null,null);