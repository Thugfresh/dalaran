import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmploymentDataService {

  api: string = 'https://datahub.io/core/employment-us/r/aat1.json'

  constructor(private http: HttpClient) { }

  get(): Observable<any> {
    return this.http.get(this.api)
  }

}
