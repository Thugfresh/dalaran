import { Component, OnInit } from '@angular/core';
import { EChartsOption } from 'echarts';
import { EmploymentDataService } from '../employment-data.service';

@Component({
  selector: 'app-graphic2',
  templateUrl: './graphic2.component.html',
  styleUrls: ['./graphic2.component.scss']
})
export class Graphic2Component implements OnInit {

  chartOption: EChartsOption = {}

  constructor(private _dataService: EmploymentDataService) { }

  ngOnInit(): void {
    this._dataService.get().subscribe(data => {
      this.chartOption = {
        title: {
          text: 'US unemployment rates since 1940',
          textAlign: "center",
          left: "50%"
        },
        grid: {
          right: "20%",
          left: "10%",
        },
        tooltip: {
          show: true,
          backgroundColor: "black",
          extraCssText: "opacity: 60%",
          textStyle: {
            color: "white"
          },
          showContent: true,
          formatter: function (data: any) {
            let pop = data[0]?.value
            let total = data[1]?.value
            let unemployed = data[2]?.value
            return Array.isArray(data) ? 
              `Population: ${pop}<br/>Employed Total: ${total}<br/>Unemployed: ${unemployed}` : ""
          }
        },
        legend: {
          show: true,
          align: "right",
          orient: "vertical",
          top: 170,
          right: 100,
          tooltip: {
            show: true
          }
        },
        xAxis: {
          name: "year",
          nameLocation: "middle",
          nameGap: 60,
          axisPointer: {
            show: true,
            type: "line",
            lineStyle: {
              type: "solid"
            }
          },
          axisLine: {
            show: true
          },
          axisLabel: {
            height: 80,
            interval: 2,
            baseline: "middle",
            align: "center",
            showMaxLabel: false,
            showMinLabel: true,
            margin: 16
          },
          axisTick: {
            show: true
          },
          scale: true,
          type: 'category',
          data: data.map((a: { year: number; }) => a.year)
        },
        yAxis: {
          name: "population",
          nameLocation: "middle",
          nameRotate: 90,
          nameGap: 80,
          type: 'value',
          minInterval: 2,
          min: 0,
          max: 250000,
          axisTick: {
            show: true
          },
          axisLine: {
            show: true
          },
          axisLabel: {
            showMinLabel: true,
            showMaxLabel: true,
          },
          axisPointer: {
            show: true,
            type: "line",
            label: {
              align: "center"
            }
          }
        },
        series: [
          {
            name: 'Population',
            showAllSymbol: false,
            showSymbol: false,
            data: data.map((a: { population: number; }) => a.population),
            type: 'line',
            color: 'black',
            lineStyle: {
              type: 'solid'
            }
          },
          {
            name: 'Employment Total',
            showAllSymbol: false,
            showSymbol: false,
            data: data.map((a: { employed_total: number; }) => a.employed_total),
            type: 'line',
            color: 'orange',
            lineStyle: {
              type: 'solid'
            }
          },
          {
            name: 'Unemployed',
            showAllSymbol: false,
            showSymbol: false,
            data: data.map((a: { unemployed: number; }) => a.unemployed),
            type: 'line',
            color: 'green',
            lineStyle: {
              type: 'solid'
            }
          }
        ],
      }
    })
  }
}
