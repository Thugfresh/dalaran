import { Component, OnInit } from '@angular/core';
import { EChartsOption, graphic, number } from 'echarts';
import { EmploymentDataService } from '../employment-data.service';

@Component({
  selector: 'app-graphic1',
  templateUrl: './graphic1.component.html',
  styleUrls: ['./graphic1.component.scss']
})
export class Graphic1Component implements OnInit {

  chartOption: EChartsOption = {}

  constructor(private _dataService: EmploymentDataService) { }

  ngOnInit(): void {
    this._dataService.get().subscribe(data => {
      this.chartOption = {
        title: {
          text: 'US unemployment rates since 1940',
          textAlign: "center",
          left: "50%",
        },
        xAxis: {
          name: "year",
          nameLocation: "middle",
          nameGap: 60,
          axisPointer: {
            show: true,
            type: "none"
          },
          axisLine: {
            show: false
          },
          axisLabel: {
            height: 60,
            interval: 0,
            baseline: "middle",
            align: "center",
            showMaxLabel: true,
            showMinLabel: false,
            margin: 12,
            formatter: function (value: any, index: number) {
              return (value % 10 == 0 && index != 0) ? value : null
            }
          },
          axisTick: {
            show: false
          },
          scale: true,
          type: 'category',
          data: data.map((a: { year: number; }) => a.year)
        },
        yAxis: {
          name: "unemployed_percent",
          nameLocation: "middle",
          nameRotate: 90,
          nameGap: 80,
          type: 'value',
          minInterval: 2,
          min: 0,
          axisTick: {
            show: false
          },
          axisLabel: {
            showMinLabel: false,
            showMaxLabel: true,
            formatter: function (value: any) {
              return value + '%'
            }
          },
          axisPointer: {
            show: true,
            type: "none",
            label: {
              align: "center",
              formatter: function (data: any) {
                let val = data.seriesData[0].value
                return val + "%"
              }
            }
          }
        },
        series: [
          {
            name: 'Unemployment',
            showAllSymbol: false,
            showSymbol: false,
            data: data.map((a: { unemployed_percent: number; }) => a.unemployed_percent),
            type: 'line',
            color: 'black',
            lineStyle: {
              type: 'solid'
            }
          },
        ],
      }
    })
  }
}
