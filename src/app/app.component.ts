import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'dalaran';

  displayingGraphic: number = 1

  graphics: any = [
    {id: 1, name: "Graphic 1"},
    {id: 2, name: "Graphic 2"}
  ]

  changeGraphic(event: any): void {
    this.displayingGraphic = event.target.value
    console.log(this.displayingGraphic)
  }

  graphicName(id: number): String {
    let graphic = this.graphics.filter((g: { id: number; }) => g.id == id)
    return graphic[0].name
  }
}
